import time
import datetime

def time2datetime(t):
    return datetime.datetime.fromtimestamp(t)

def datetime2time(dt):
    return time.mktime(dt.timetuple())
