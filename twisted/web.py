from twisted.internet import defer
from twisted.web import client


@defer.inlineCallbacks
def get_page(url):
    html = yield client.getPage(url)
    defer.returnValue(html)

