from twisted.internet import defer

from pyquery import PyQuery as pq


@defer.inlineCallbacks
def parse_document(html):
    d = yield pq(html)
    defer.returnValue(d)


@defer.inlineCallbacks
def get_elements(d, query):
    elements = yield d(query)
    defer.returnValue(elements)


@defer.inlineCallbacks
def get_attr(el, attr):
    res = yield pq(el).attr(attr)
    defer.returnValue(res)
